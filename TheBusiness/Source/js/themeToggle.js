// This function will toggle the two class names of each body selected.

function themeToggle() {

    $('body').toggleClass('darkTheme');
    $('body').toggleClass('lightTheme');
    $('html').toggleClass('darkTheme');
    $('html').toggleClass('lightTheme');
    $('nav').toggleClass('darkTheme');
    $('nav').toggleClass('lightTheme');

}